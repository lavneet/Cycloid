#include <fstream>
#include <cmath>
#include <string>

using namespace std;

#include "Cycloid.h"

class FileOpeningException {
	string errorMessage;
public:
	FileOpeningException(string message) {
		errorMessage = message;
	}
	string getMessage() {
		return errorMessage;
	}
};

Cycloid::Cycloid() {
  x_vals = y_vals = 0;

}
Cycloid::Cycloid( double a, double b, int resolution ) {

  start = a; end = b;
  num_div = resolution;

  x_vals = new double[num_div+1];
  createIntervals();

  y_vals = new double[num_div+1];

}

Cycloid::~Cycloid() {
  delete[] x_vals;
  delete[] y_vals;

}

void Cycloid::createIntervals( void ) {

  double dx = ( end - start ) / num_div;

  for (int i = 0; i <= num_div; i++) {
    x_vals[i] = start + i*dx;
  }

}

double Cycloid::calcSinx( double x, int n ) {

	if ( n == 0 ) return sin(1);

	return sin( x + calcSinx(x, n-1) );

}

double Cycloid::calculate( double x, int n ) {

  return ( cos( x + calcSinx(x, n) ) + 1 );

}



void Cycloid::render( int n ) {

  for (int i = 0; i <= num_div; i++) {
    y_vals[i] = calculate( x_vals[i], n );

  }

}

void Cycloid::exportData( string filename ) {

  ofstream outputStream(filename.c_str());

	if (outputStream.fail()) {
		throw new FileOpeningException("Error opening " + filename);
	}

	for (int i = 0; i <= num_div; i++) {
		outputStream << x_vals[i] << " " << y_vals[i] << endl;
	}

	outputStream.close();

}

void Cycloid::updateDomain( double a, double b ) {

  start = a; end = b;
  createIntervals();

}
