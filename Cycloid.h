#pragma once

#include <string>

class Cycloid {

  double *x_vals,
         *y_vals;

  double start, end;
  int num_div;

  //Set up x-axis:
  void createIntervals( void );

  //Approximate value of sin(x+sin(x+sin(x+sin(...)))) at 'x':
  double calcSinx( double x, int n );
  //Approximate output at 'x':
  double calculate( double x, int n );

  public:
    Cycloid();
    Cycloid( double a, double b, int resolution );
    ~Cycloid();

    //Assign output to y-axis:
    void render( int n );
    //Export (x,y) coordinates to file:
    void exportData( string filename );


    void updateDomain( double a, double b );

};
