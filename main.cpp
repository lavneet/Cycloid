#include <cmath>

using namespace std;

#include "Cycloid.h"

int main( void ) {

  Cycloid* cycloid = new Cycloid( 0, M_PI, 100 );

  cycloid->render( 100 );

  cycloid->exportData( "plot_data.txt" );

  delete cycloid;
  return 0;
}
