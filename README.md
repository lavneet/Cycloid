A simple C++ visualization illustrating the convergence of a recurrence function into Cycloid.

Refer to https://www.desmos.com/calculator/kduagmsvgo for context and an interactive playground.